# Zadanie rekurtacyjne dla XTM 

## Projekt Maven 

- Projekt w oparciu o Springboot;
- REST API w oparciu o restTemplate;
- Do utrwalania danych implementacja bazy danych w pamięci H2;
- Testy jednostkowe, brak testów integracyjnych ze względu na implementację warstwy persystencji tylko w oparciu o TranslationDao;
- Konfiguracja w pliku application.properties, domyślnie ustawiono port 47144.

# Endpointy
## Tłumaczenie dla podanego słowa: 
- localhost:47144/translate?word={słowo}
## Podsumowanie dla wszystkich słów:
- localhost:47144/stats

# Zbudowanie i uruchomienie
- mvn clean install
- mvn spring-boot:run

# Celowo endpointy zwracają zwykły string, nie JSON - tak zrozumiałem zadanie.
