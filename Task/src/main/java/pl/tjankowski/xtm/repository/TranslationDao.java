package pl.tjankowski.xtm.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import pl.tjankowski.xtm.dto.StatsDto;
import pl.tjankowski.xtm.dto.TranslationDto;

@Repository
public class TranslationDao {

	Logger logger = LoggerFactory.getLogger(TranslationDao.class);

	final String statsQuery = "SELECT WORD, count(*) AS COUNT FROM TRANSLATION t group by WORD;";
	final String allTranslationsQuery = "SELECT * FROM TRANSLATION t;";
	final String insertTranslationQuery = "INSERT INTO TRANSLATION (WORD, TRANSLATION, CREATION_TIME) VALUES (?, ?, ?);";
	final String clearQuery = "DELETE FROM TRANSLATION;";
	final String countQuery = "SELECT count(*) as COUNT from TRANSLATION;";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public void save(TranslationDto translation) {
		logger.info("Zapis tłumaczenia: {}.", translation.getWord());
		jdbcTemplate.update(insertTranslationQuery, translation.getWord(), translation.getTranslation(), new Date());
	}

	public List<TranslationDto> loadAll() {
		logger.info("Pobieranie wszystkich wyszukiwanych tłumaczeń.");
		List<TranslationDto> result = jdbcTemplate.query(allTranslationsQuery, (resultSet, i) -> {
			return toTranslation(resultSet);
		});
		logger.info("Łącznie tłumaczeń: {}.", result.size());
		return result;
	}

	public List<StatsDto> getStats() {
		logger.info("Pobieranie statystyk.");
		List<StatsDto> stats = jdbcTemplate.query(statsQuery, (resultSet, i) -> {
			return toStats(resultSet);
		});
		sortStats(stats);
		return stats;
	}

	public void clearTranslations() {
		logger.info("Czyszczenie bazy danych.");
		jdbcTemplate.update(clearQuery);
	}

	private StatsDto toStats(ResultSet resultSet) throws SQLException {
		return new StatsDto(resultSet.getString("WORD"), resultSet.getLong("COUNT"));
	}

	private TranslationDto toTranslation(ResultSet resultSet) throws SQLException {
		TranslationDto translation = new TranslationDto();
		translation.setId(resultSet.getLong("ID"));
		translation.setWord(resultSet.getString("WORD"));
		translation.setTranslation(resultSet.getString("TRANSLATION"));
		translation.setCreationTime(resultSet.getDate("CREATION_TIME"));
		return translation;
	}

	private void sortStats(List<StatsDto> stats) {
		stats.sort((s1, s2) -> s2.getCount().compareTo(s1.getCount()));
	}
}