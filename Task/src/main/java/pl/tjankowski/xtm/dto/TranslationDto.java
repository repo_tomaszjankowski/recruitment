package pl.tjankowski.xtm.dto;

import java.util.Date;

public class TranslationDto {

	private Long id;

	private String word;

	private String translation;

	private Date creationTime;

	public TranslationDto() {
		super();
	}

	public TranslationDto(Long id, String word, String translation, Date creationTime) {
		super();
		this.id = id;
		this.word = word;
		this.translation = translation;
		this.creationTime = creationTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getTranslation() {
		return translation;
	}

	public void setTranslation(String translation) {
		this.translation = translation;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public String toString() {
		return "TranslationDto [id=" + id + ", word=" + word + ", translation=" + translation + ", creationTime="
				+ creationTime + "]";
	}

}