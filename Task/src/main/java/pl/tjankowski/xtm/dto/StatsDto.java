package pl.tjankowski.xtm.dto;

public class StatsDto {

	private String word;
	private Long count;

	public StatsDto(String word, Long count) {
		super();
		this.word = word;
		this.count = count;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "StatsDto [word=" + word + ", count=" + count + "]";
	}

}
