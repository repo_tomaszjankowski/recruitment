package pl.tjankowski.xtm.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
@EnableAutoConfiguration
public class TranslatorApplication implements CommandLineRunner {

	Logger logger = LoggerFactory.getLogger(TranslatorApplication.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(TranslatorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String createSql = "DROP TABLE IF EXISTS TRANSLATION; "
				+ "CREATE TABLE TRANSLATION (ID INT AUTO_INCREMENT PRIMARY KEY, " 
				+ "WORD VARCHAR(250) NOT NULL, "
				+ "TRANSLATION VARCHAR(250) NULL," 
				+ "CREATION_TIME timestamp DEFAULT NULL);";
		jdbcTemplate.execute(createSql);

		logger.info("Database ready.");
	}

}
