package pl.tjankowski.xtm.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handle(Exception ex, HttpServletRequest request, HttpServletResponse response) {
		if (ex instanceof NullPointerException) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else if (ex instanceof JSONException) {
			return new ResponseEntity<>(new Error("Problem z tłumaczeniem lub nie znaleziono tłumaczenia!"),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);

		}
	}
}