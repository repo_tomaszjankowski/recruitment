package pl.tjankowski.xtm.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

@Service
public class DictionaryService implements DictionaryServiceInterface {

	private final String dictionaryFilePath = "dictionary.json";

	@Override
	public JSONObject getJSONObject() throws JSONException, IOException {
		File resource = new ClassPathResource(dictionaryFilePath).getFile();
		JSONObject jsonObj = new JSONObject(new String(Files.readAllBytes(resource.toPath())));

		return jsonObj;
	}

}
