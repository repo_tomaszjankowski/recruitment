package pl.tjankowski.xtm.service;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

public interface DictionaryServiceInterface {

	public JSONObject getJSONObject() throws JSONException, IOException;
}
