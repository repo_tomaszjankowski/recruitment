package pl.tjankowski.xtm.controller;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pl.tjankowski.xtm.dto.StatsDto;
import pl.tjankowski.xtm.dto.TranslationDto;
import pl.tjankowski.xtm.repository.TranslationDao;
import pl.tjankowski.xtm.service.DictionaryService;

@RestController
public class TranslationController {

	Logger logger = LoggerFactory.getLogger(TranslationController.class);

	@Autowired
	DictionaryService dictionaryService;

	@Autowired
	TranslationDao translationDao;

	@GetMapping("/translate")
	public String translate(@RequestParam(value = "word", defaultValue = "") String word)
			throws JSONException, IOException {
		String result = "";

		try {
			result = dictionaryService.getJSONObject().get(word).toString();
			logger.info("Wynik tłumaczenia dla: {} to: {}.", word, result);
			translationDao.save(new TranslationDto(null, word, result, null));
		} catch (JSONException e) {
			result = String.format("\"%s\"", word);
			logger.info("Wynik tłumaczenia (brak w słowniku) dla: {} to: {}.", word, result);
		} catch (IOException e) {
			logger.error("Błąd tłumaczenia!", e);
		}

		return result;
	}

	@GetMapping("/stats")
	public List<StatsDto> stats() {
		return translationDao.getStats();
	}

}
