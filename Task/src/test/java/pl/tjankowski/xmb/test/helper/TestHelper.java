package pl.tjankowski.xmb.test.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;

import pl.tjankowski.xtm.dto.StatsDto;
import pl.tjankowski.xtm.dto.TranslationDto;

public class TestHelper {

	public static Date testDate = new Date();

	public static List<TranslationDto> translatonList() throws JSONException, IOException {
		List<TranslationDto> list = new ArrayList<TranslationDto>();
		list.add(customTranslation("word-1", "translation-1"));
		list.add(customTranslation("word-1", "translation-1"));
		list.add(customTranslation("word-1", "translation-1"));
		list.add(customTranslation("word-1", "translation-1"));
		list.add(customTranslation("word-2", "translation-2"));
		list.add(customTranslation("word-2", "translation-2"));
		list.add(customTranslation("word-2", "translation-2"));
		list.add(customTranslation("word-3", "translation-3"));
		list.add(customTranslation("word-3", "translation-3"));
		list.add(customTranslation("word-4", "translation-4"));

		return list;
	}

	public static TranslationDto customTranslation(String word, String translation) {
		return new TranslationDto(null, word, translation, testDate);
	}

	public static TranslationDto customTranslationWithDate(String word, String translation, Date date) {
		return new TranslationDto(null, word, translation, date);
	}

	public static List<StatsDto> statsList() {
		List<StatsDto> stats = new ArrayList<StatsDto>();
		for (int i = 0; i <= 10; i++) {
			stats.add(new StatsDto("word-" + i, Long.valueOf(i * 10)));
		}
		stats.sort((s1, s2) -> s2.getCount().compareTo(s1.getCount()));
		return stats;
	}
	
	public static Map<String, String> dictionaryMap() {
		Map<String, String> dictionaryMap = new HashMap<>();
		dictionaryMap.put("Ala", "Alice");
		dictionaryMap.put("ma", "has");
		dictionaryMap.put("kota", "a cat");
		dictionaryMap.put("jesteś", "you are");
		dictionaryMap.put("sterem", "the helm");
		dictionaryMap.put("białym", "white");
		dictionaryMap.put("żołnierzem", "soldier");
		dictionaryMap.put("nosisz", "wear");
		dictionaryMap.put("spodnie", "trousers");
		dictionaryMap.put("więc", "so");
		dictionaryMap.put("walcz", "fight");
		return dictionaryMap;
	}
}
