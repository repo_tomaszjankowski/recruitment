package pl.tjankowski.xmb.test;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import pl.tjankowski.xmb.test.helper.TestHelper;
import pl.tjankowski.xtm.dto.StatsDto;
import pl.tjankowski.xtm.dto.TranslationDto;
import pl.tjankowski.xtm.repository.TranslationDao;
import pl.tjankowski.xtm.service.DictionaryService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { pl.tjankowski.xtm.main.TranslatorApplication.class })
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class TranslatorTest {

	Logger logger = LoggerFactory.getLogger(TranslatorTest.class);

	private Date testDate = new Date();

	@Autowired
	private MockMvc mvc;

	@Autowired
	private DictionaryService dictionaryService;

	@Autowired
	private TranslationDao repository;

	@Test
	public void contextLoads() throws Exception {
		Assert.assertNotNull(repository);
	}

	@Test
	public void saveAndLoadTest() {
		logger.info("Test zapisu i odczytu bazy danych.");
		TranslationDto translation = TestHelper.customTranslation("testWord", "testTranslation");
		repository.save(translation);
		Assert.assertEquals(1, totalNumberOfTranslations());
		String savedWord = repository.loadAll().get(0).getWord();
		logger.info("Tekst zapisany w bazie: {}.", savedWord);
		Assert.assertEquals(translation.getWord(), savedWord);
	}

	@Test
	public void databaseClearTest() {
		logger.info("Test czyszczenia bazy danych.");
		TranslationDto translation = TestHelper.customTranslation("toClearWord", "toClearTranslation");
		Long translationsBeforeTest = Long.valueOf(totalNumberOfTranslations());
		repository.save(translation);
		Long translationsAfterTest = Long.valueOf(totalNumberOfTranslations());
		Assert.assertNotEquals(translationsBeforeTest, translationsAfterTest);
		repository.clearTranslations();
		Long translationsAfterClearTest = Long.valueOf(totalNumberOfTranslations());
		Assert.assertEquals(0, translationsAfterClearTest.longValue());
	}

	@Test
	public void statsTest() throws JSONException, IOException {
		logger.info("Test statystyk bazy danych.");
		TestHelper.translatonList().stream().forEach(x -> repository.save(x));
		List<StatsDto> stats = repository.getStats();
		StatsDto firstStat = stats.stream().findFirst().get();
		StatsDto lastStat = stats.get(stats.size() - 1);
		logger.info("{} {}", firstStat, lastStat);
		Assert.assertTrue(firstStat.getCount() >= lastStat.getCount());
	}

	private int totalNumberOfTranslations() {
		return repository.loadAll().size();
	}

}