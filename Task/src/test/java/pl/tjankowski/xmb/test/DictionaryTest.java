package pl.tjankowski.xmb.test;

import java.io.IOException;
import java.util.Map.Entry;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import pl.tjankowski.xmb.test.helper.TestHelper;
import pl.tjankowski.xtm.service.DictionaryService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { pl.tjankowski.xtm.main.TranslatorApplication.class })
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class DictionaryTest {

	Logger logger = LoggerFactory.getLogger(DictionaryTest.class);

	final String[] dictionaryKeys = { "Ala", "ma", "kota", "walcz" };

	@Autowired
	private DictionaryService dictionaryService;

	@Test
	public void dictionaryFromJsonTest() throws IOException, JSONException {
		logger.info("Test słownika z obiektu JSON.");
		JSONObject dictionaryJsonObject = dictionaryService.getJSONObject();
		for (String key : dictionaryKeys) {
			logger.info("Klucz: {}, wartość: {}", key, dictionaryJsonObject.get(key));
		}
	}

}
